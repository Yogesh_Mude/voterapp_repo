function validate(frm)
{
	frm.vflag.value="yes";
	//read form data
	var un=frm.pname.value;
	var age=frm.page.value;
	//perform client side form validation
	if(un=="")
	{
		alert("person name is mandatory");
		frm.pname.focus();
		return false;
	}
	if(age=="")
	{
		alert("person age is mandatory");
		frm.page.focus();
		return false;
	}
	else
	{
		if(isNaN(age))  //check  whether age is numeric value or not
		{
			alert("page  age must be numeric value");
			frm.pname.focus();
			frm.page.focus();
			return false;
		}//if
	}//else
	return true;
}//function