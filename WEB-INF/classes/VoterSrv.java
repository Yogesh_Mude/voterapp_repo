package com.ds;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class VoterSrv extends HttpServlet
{
	public void xyz(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
	{
		//general settings
		PrintWriter pw=res.getWriter();
		res.setContentType("text/html");

		//read form data from form page
		String name=req.getParameter("pname");
		String tage=req.getParameter("page");
		
		int age=0;
		String vstatus=req.getParameter("vflag");  //get client side validation status
		if(vstatus.equals("no"))  //if client side form validation are not done
		{
		//server side form validation
		if(name.equals("")||name==null||name.length()==0)
		{
			pw.println("<font color='red'>Person name is Mandatory</font>");
			return;
		}
		if(tage.equals("")||tage==null||tage.length()==0)
		{
			pw.println("<font color='red'>Person age is Mandatory</font>");
			return;
		}
		else  // to check age is numeric value or not
		{
			try
			{
				//convert given age value to numeric value
				age=Integer.parseInt(tage);
			}
			catch (NumberFormatException nfe)
			{
				pw.println("<font color='red'>Age must be numeric value</font>");
				return;
			}
		}//else
		System.out.println("Server side form validation is completed");
	}//if

	if(vstatus.equals("yes"))  //when client side form validation are done
	{
		age=Integer.parseInt(tage);
	}
	//Write request processing logic
	if(age>=18)
		pw.println("<b><i><font color='green'>You are Elgible For Vote</i></b>");
	else
		pw.println("<b><i><font color='red'>You are not Elgible For Vote</i></b>");

	pw.println("<br><br><a href='input.html'><img src='1.jpg'width=50 height=50</a>");

	//close stream
	pw.close();
   }//doGet()

public void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
{
	System.out.println("VoterSrv.doGet()");
	xyz(req,res);
}

public void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
{
	System.out.println("VoterSrv.doPost()");
	xyz(req,res);
}

}//class